package com.prakash.trivia.repository

import com.prakash.trivia.model.Details


interface MainRepository {

    suspend fun insertDetails(details: Details)

    suspend fun getAllDetails(): List<Details>
}