package com.prakash.trivia.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import com.prakash.trivia.R
import com.prakash.trivia.model.Details
import com.prakash.trivia.util.CommonUtil
import com.prakash.trivia.util.Constants
import kotlinx.android.synthetic.main.item_summary.view.*

class HistoryListAdapter() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Details>() {

        override fun areItemsTheSame(oldItem: Details, newItem: Details): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Details, newItem: Details): Boolean {
            return oldItem == newItem
        }

    }
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return HistoryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_summary,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HistoryViewHolder -> {
                holder.bind(differ.currentList[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<Details>) {
        differ.submitList(list)
    }

    class HistoryViewHolder
    constructor(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Details) = with(itemView) {

            val date = CommonUtil.getFormattedStringFromDate(Constants.DATE_FORMAT, item.date)
            tvSummary.text = "GAME ${adapterPosition+1}: $date"
            tvTitle.text = resources.getString(R.string.name_s, item.name)
            tvAns1.text = resources.getString(R.string.answer1_s, item.bestCricketer)
            tvAns2.text = resources.getString(R.string.answer2_s, item.indianFlagColors)

        }
    }
}