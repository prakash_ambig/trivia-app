package com.prakash.trivia.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.prakash.trivia.R
import com.prakash.trivia.ui.MainActivity
import com.prakash.trivia.ui.adapter.OptionListAdapter
import com.prakash.trivia.util.Constants
import com.prakash.trivia.viewModel.MainViewModel
import kotlinx.android.synthetic.main.fragment_trivia.*
import kotlinx.android.synthetic.main.item_name.*
import kotlinx.android.synthetic.main.item_selection.*
import kotlinx.android.synthetic.main.item_summary.*

class TriviaFragment : Fragment(R.layout.fragment_trivia) {
    private lateinit var viewModel: MainViewModel
    private lateinit var optionAdapter: OptionListAdapter

    private var position: Int = 0

    companion object {
        fun getInstance(bundle: Bundle): TriviaFragment {
            val fragment = TriviaFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            position = it.getInt(Constants.EXTRA_PAGE_NUMBER)
        }
        initViewModel()
        onViewClick()
        loadViews()
    }

    private fun initViewModel() {
        if (activity is MainActivity) {
            viewModel = (activity as MainActivity).viewModel
        }
    }

    private fun onViewClick() {
        btnNext.setOnClickListener {
            if (position == 3) {
                viewModel.onFinish()
            } else {
                val name = etName.text.toString()
                if (position == 0 && name.isEmpty()) {
                    Toast.makeText(requireContext(), "Please enter name", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                viewModel.onNext(name, position == 2)
            }
        }

        btnHistory.setOnClickListener {
            findNavController().navigate(R.id.detailsFragment)
        }
    }

    private fun loadViews() {
        when (position) {
            0 -> {
                layoutName.visibility = View.VISIBLE
                layoutOption.visibility = View.GONE
                layoutSummary.visibility = View.GONE
                btnHistory.visibility = View.GONE
                observeName()
            }
            1 -> {
                tvQuestion.text = getString(R.string.who_is_best_cricketer)
                layoutName.visibility = View.GONE
                layoutOption.visibility = View.VISIBLE
                layoutSummary.visibility = View.GONE
                btnHistory.visibility = View.GONE
                setUpAdapter()
                observeOption2()
            }
            2 -> {
                tvQuestion.text = getString(R.string.what_are_the_colors_in_indian_flag)
                layoutName.visibility = View.GONE
                layoutOption.visibility = View.VISIBLE
                layoutSummary.visibility = View.GONE
                btnHistory.visibility = View.GONE
                setUpAdapter()
                observeOption3()
            }
            3 -> {
                layoutName.visibility = View.GONE
                layoutOption.visibility = View.GONE
                layoutSummary.visibility = View.VISIBLE
                btnHistory.visibility = View.VISIBLE
                btnNext.text = getString(R.string.finish)
                observeSummary()
            }

        }
    }

    private fun setUpAdapter() {
        optionAdapter = OptionListAdapter(isMultiSelection = position == 2)

        optionRecycler.apply {
            adapter = optionAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observeName() {
        viewModel.name.observe(viewLifecycleOwner, {
            etName.setText(it)
        })
    }

    private fun observeOption2() {
        viewModel.option2List.observe(viewLifecycleOwner, {
            optionAdapter.submitList(it)
        })
    }

    private fun observeOption3() {
        viewModel.option3List.observe(viewLifecycleOwner, {
            optionAdapter.submitList(it)
        })
    }

    private fun observeSummary() {
        viewModel.detailObj.observe(viewLifecycleOwner, {
            it?.let {
                tvTitle.text = getString(R.string.title_s, it.name)
                tvAns1.text = getString(R.string.answer1_s, it.bestCricketer)
                tvAns2.text = getString(R.string.answer2_s, it.indianFlagColors)
            }
        })
    }

}