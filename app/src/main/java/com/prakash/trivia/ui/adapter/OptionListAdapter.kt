package com.prakash.trivia.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.prakash.trivia.R
import com.prakash.trivia.model.Option
import kotlinx.android.synthetic.main.item_option.view.*

class OptionListAdapter(
    private var data: List<Option> = emptyList(),
    private val isMultiSelection: Boolean = false
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return OptionViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_option,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is OptionViewHolder -> {
                holder.bind(data[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun submitList(list: List<Option>) {
        data = list
        notifyDataSetChanged()
    }

    inner class OptionViewHolder
    constructor(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Option) = with(itemView) {
            itemView.setOnClickListener {
                if (isMultiSelection) {
                    item.isSelected = !item.isSelected
                } else {
                    data.map { it.isSelected = false }
                    item.isSelected = true
                }
                notifyDataSetChanged()
            }

            radioBtn.setOnClickListener { itemView.performClick() }

            radioBtn.isChecked = item.isSelected
            tvOption.text = item.option

        }
    }

}