package com.prakash.trivia.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.prakash.trivia.model.Details
import com.prakash.trivia.util.Constants

@Dao
interface DetailsDao {

    @Insert
    suspend fun insertDetails(details: Details)

    @Query("SELECT * FROM ${Constants.TABLE_NAME_DETAILS}")
    suspend fun getAllDetails(): List<Details>

}