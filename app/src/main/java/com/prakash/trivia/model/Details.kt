package com.prakash.trivia.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.prakash.trivia.util.Constants
import java.util.*

@Entity(tableName = Constants.TABLE_NAME_DETAILS)
data class Details(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name: String = "",
    val bestCricketer: String = "",
    val indianFlagColors: String = "",
    var date: Date? = null
)