package com.prakash.trivia.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.prakash.trivia.model.Details
import com.prakash.trivia.util.Constants

@Database(
    entities = [Details::class],
    version = Constants.DATABASE_VERSION,
    exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun postDao(): DetailsDao
}