package com.prakash.trivia.ui.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.prakash.trivia.ui.fragment.TriviaFragment
import com.prakash.trivia.util.Constants

class TriviaPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    override fun getItemCount(): Int {
        return Constants.NUM_PAGES
    }

    override fun createFragment(position: Int): Fragment {
        val bundle = Bundle()
        bundle.putInt(Constants.EXTRA_PAGE_NUMBER, position)
        return TriviaFragment.getInstance(bundle)
    }
}