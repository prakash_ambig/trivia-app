package com.prakash.trivia.util

import android.text.format.DateFormat
import java.util.*

object CommonUtil {
    fun getCurrentDateTime() : Date {
        return Calendar.getInstance().time
    }

    fun getFormattedStringFromDate(format: String, date: Date?): String? {
        return date?.let { DateFormat.format(format, date) as String }
    }
}