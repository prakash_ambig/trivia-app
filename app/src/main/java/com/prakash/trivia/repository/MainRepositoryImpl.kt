package com.prakash.trivia.repository

import com.prakash.trivia.database.DetailsDao
import com.prakash.trivia.model.Details
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val dao: DetailsDao
) : MainRepository {

    override suspend fun insertDetails(details: Details) {
        dao.insertDetails(details)
    }

    override suspend fun getAllDetails(): List<Details> {
        return dao.getAllDetails()
    }


}