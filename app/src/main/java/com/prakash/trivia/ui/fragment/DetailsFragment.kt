package com.prakash.trivia.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.prakash.trivia.R
import com.prakash.trivia.ui.MainActivity
import com.prakash.trivia.ui.adapter.HistoryListAdapter
import com.prakash.trivia.viewModel.MainViewModel
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : Fragment(R.layout.fragment_details) {
    private lateinit var viewModel: MainViewModel
    private lateinit var historyListAdapter: HistoryListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        setUpListAdapter()
        subscribeObservers()
    }

    private fun initViewModel() {
        if (activity is MainActivity) {
            viewModel = (activity as MainActivity).viewModel
            viewModel.getAllDetails()
        }
    }

    private fun subscribeObservers() {
        viewModel.allDetails.observe(viewLifecycleOwner, {
            historyListAdapter.submitList(it)
        })
    }

    private fun setUpListAdapter() {
        historyListAdapter = HistoryListAdapter()

        detailsRecycler.apply {
            adapter = historyListAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }
}