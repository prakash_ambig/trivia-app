package com.prakash.trivia.di

import android.content.Context
import androidx.room.Room
import com.prakash.trivia.database.AppDatabase
import com.prakash.trivia.database.DetailsDao
import com.prakash.trivia.repository.MainRepository
import com.prakash.trivia.repository.MainRepositoryImpl
import com.prakash.trivia.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideMainRepository(
        dao: DetailsDao
    ) = MainRepositoryImpl(dao) as MainRepository

    @Singleton
    @Provides
    fun provideAppDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(context, AppDatabase::class.java, Constants.DATABASE_NAME).build()

    @Singleton
    @Provides
    fun providePostDao(
        database: AppDatabase
    ) = database.postDao()
}