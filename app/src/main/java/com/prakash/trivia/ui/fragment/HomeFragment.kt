package com.prakash.trivia.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.prakash.trivia.R
import com.prakash.trivia.ui.MainActivity
import com.prakash.trivia.ui.adapter.TriviaPagerAdapter
import com.prakash.trivia.util.EventObserver
import com.prakash.trivia.viewModel.MainViewModel
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment(R.layout.fragment_home) {
    private lateinit var viewModel: MainViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        setUpViewPager()
        subscribeObservers()
    }

    private fun initViewModel() {
        if (activity is MainActivity) {
            viewModel = (activity as MainActivity).viewModel
        }
    }

    private fun setUpViewPager() {
        val pagerAdapter = TriviaPagerAdapter(requireActivity())
        pager.adapter = pagerAdapter
    }

    private fun subscribeObservers() {
        viewModel.nextEvent.observe(viewLifecycleOwner, EventObserver {
            val nextPage = if (it) 0 else pager.currentItem + 1
            pager.currentItem = nextPage
        })
    }

}