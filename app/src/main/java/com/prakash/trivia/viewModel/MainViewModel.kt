package com.prakash.trivia.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.prakash.trivia.model.Details
import com.prakash.trivia.model.Option
import com.prakash.trivia.repository.MainRepository
import com.prakash.trivia.util.CommonUtil
import com.prakash.trivia.util.Event
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val repository: MainRepository
) : ViewModel() {

    private val _nextEvent = MutableLiveData<Event<Boolean>>()
    val nextEvent: LiveData<Event<Boolean>> = _nextEvent

    private val _detailObj = MutableLiveData<Details>()
    val detailObj: LiveData<Details> = _detailObj

    private val _name = MutableLiveData<String>()
    val name: LiveData<String> = _name

    private val _allDetails = MutableLiveData<List<Details>>()
    val allDetails: LiveData<List<Details>> = _allDetails

    private var details: Details? = null
    private var userName: String = ""

    fun onNext(name: String, isSubmit: Boolean) {
        if (name.isNotEmpty()) {
            userName = name
        }
        _nextEvent.value = Event(false)
        if (isSubmit) {
            saveDetails()
        }
    }

    fun onFinish() {
        option2.map { it.isSelected = false }
        option3.map { it.isSelected = false }
        _nextEvent.value = Event(true)
        _option2List.postValue(option2)
        _option3List.postValue(option3)
        _detailObj.postValue(Details())
        _name.postValue("")
    }

    private val option2 = listOf(
        Option("Sachin Tendulkar"),
        Option("Virat Kohli"),
        Option("Adam Gilchirst"),
        Option("Jacques Kallis")
    )

    private val option3 = listOf(
        Option("White"),
        Option("Yellow"),
        Option("Orange"),
        Option("Green")
    )

    private val _option2List = MutableLiveData(option2)
    val option2List: LiveData<List<Option>> = _option2List

    private val _option3List = MutableLiveData(option3)
    val option3List: LiveData<List<Option>> = _option3List

    private fun saveDetails() {
        val ans2 = option2.find { it.isSelected }?.option ?: ""
        val ans3 = option3.filter(Option::isSelected).joinToString { it.option }
        details = Details(name = userName, bestCricketer = ans2, indianFlagColors = ans3, date = CommonUtil.getCurrentDateTime())
        _detailObj.postValue(details)
        details?.let {
            viewModelScope.launch {
                repository.insertDetails(it)
            }
        }
    }

    fun getAllDetails() {
        viewModelScope.launch {
            val result = repository.getAllDetails()
            _allDetails.postValue(result)
        }
    }

}