package com.prakash.trivia.model

data class Option(
    val option: String,
    var isSelected: Boolean = false
) {
}