package com.prakash.trivia.util

class Constants {

    companion object {

        const val DATABASE_NAME = "trivia_db"
        const val DATABASE_VERSION = 1
        const val NUM_PAGES = 4

        const val TABLE_NAME_DETAILS = "trivia_details"
        const val EXTRA_PAGE_NUMBER = "page_number"
        const val DATE_FORMAT = "yyy-MMM-dd hh:mm a"
    }
}